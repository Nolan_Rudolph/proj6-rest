# Laptop Service
import os, acp_times, logging, flask, json, csv
from flask import Flask, render_template, request, session, redirect, url_for, Response
from flask_restful import Resource, Api, reqparse, fields, marshal_with
from pymongo import MongoClient

# Instantiate the app
app = Flask(__name__)
api = Api(app)

app.secret_key = '56721329980543265787809923'
app.config['SESSION_TYPE'] = 'filesystem'

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.myDB

##########  WEBPAGES  ##########


@app.route('/', methods=['GET', 'POST'])
def index():
    session['begin_date'] = "2017-01-01"
    session['begin_time'] = "00:00"
    session['total_distance'] = 200
    return render_template('index.html')


@app.route('/new', methods=['POST'])
def new():

    app.logger.debug("session['begin_date'] = " + session['begin_date'])
    app.logger.debug("session['begin_time'] = " + session['begin_time'])
    app.logger.debug("session['total_distance'] = " + str(session['total_distance']))

    app.logger.debug("DATABASE LOGS:")
    _items = db.tododb.find()
    for item in _items:
        app.logger.debug(item)

    use_date = session['begin_date'] + "T" + session['begin_time']
    distance = int(request.form['distance'])
    total_distance = session['total_distance']

    if distance > total_distance:
        return redirect(url_for('index'))

    open_time = acp_times.open_time(distance, total_distance, use_date)
    close_time = acp_times.close_time(distance, total_distance, use_date)

    ret_open_time = "{}/{} {}:{}".format(open_time[5:7], open_time[8:10],
                                         open_time[11:13], open_time[14:16])

    ret_close_time = "{}/{} {}:{}".format(close_time[5:7], close_time[8:10],
                                         close_time[11:13], close_time[14:16])

    item_doc = {
        'open_time': ret_open_time,
        'close_time': ret_close_time,
        'name': request.form['name'],
        'description': request.form['description'],
        'distance': request.form['distance']
    }
    db.myDB.insert_one(item_doc)

    app.logger.debug("DATABASE LOGS:")
    _items = db.myDB.find()
    for item in _items:
        app.logger.debug(item)

    return redirect(url_for('index'))


##########  RESTFUL API  ##########

class OpenClose(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument("top")
        args = parser.parse_args()
        k = args["top"]

        _items = db.myDB.find()
        items = [item for item in _items]

        open = []
        close = []

        if k != None:
            if int(k) > len(items):
                app.logger.debug("here")
                for item in items:
                    open.append(item['open_time'])
                    close.append(item['close_time'])
            else:
                stop = 0
                for item in items:
                    if stop < int(k):
                        open.append(item['open_time'])
                        close.append(item['close_time'])
                        stop += 1
                    else:
                        break
        else:
            for item in items:
                open.append(item['open_time'])
                close.append(item['close_time'])

        return {'Open': open, 'Close': close}


class OpenCloseCSV(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument("top")
        args = parser.parse_args()
        k = args["top"]

        _items = db.myDB.find()
        items = [item for item in _items]

        ret_string = "Open: \n"

        # Open Times

        if k != None:
            if int(k) > len(items):
                for item in items:
                    temp = item['open_time'].split(" ")
                    ret_string += (temp[0] + ',' + temp[1] + os.linesep)
            else:
                stop = 0
                for item in items:
                    if stop < int(k):
                        temp = item['open_time'].split(" ")
                        ret_string += (temp[0] + ',' + temp[1] + os.linesep)
                        stop += 1
                    else:
                        break;

            ret_string += "\nClose: \n"

            # Close Times

            if int(k) > len(items):
                for item in items:
                    temp = item['close_time'].split(" ")
                    ret_string += (temp[0] + ',' + temp[1] + os.linesep)
            else:
                stop = 0
                for item in items:
                    if stop < int(k):
                        temp = item['close_time'].split(" ")
                        ret_string += (temp[0] + ',' + temp[1] + os.linesep)
                        stop += 1
                    else:
                        break;
        else:
            for item in items:
                temp = item['open_time'].split(" ")
                ret_string += (temp[0] + ',' + temp[1] + os.linesep)

            ret_string += "\nClose: \n"

            for item in items:
                temp = item['close_time'].split(" ")
                ret_string += (temp[0] + ',' + temp[1] + os.linesep)

        return Response(ret_string, mimetype="text/csv")


class OpenCloseJSON(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument("top")
        args = parser.parse_args()
        k = args["top"]

        _items = db.myDB.find()
        items = [item for item in _items]

        open = []
        close = []

        if k != None:
            if int(k) > len(items):
                app.logger.debug("here")
                for item in items:
                    open.append(item['open_time'])
            else:
                stop1 = 0
                for item in items:
                    if stop1 < int(k):
                        open.append(item['open_time'])
                        stop1 += 1
                    else:
                        break

            if int(k) > len(items):
                app.logger.debug("here")
                for item in items:
                    close.append(item['close_time'])
            else:
                stop2 = 0
                for item in items:
                    if stop2 < int(k):
                        close.append(item['close_time'])
                        stop2 += 1
                    else:
                        break
        else:
            for item in items:
                open.append(item['open_time'])
                close.append(item['close_time'])

        return {'Open': open, 'Close': close}


class Open(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument("top")
        args = parser.parse_args()
        k = args["top"]

        _items = db.myDB.find()
        items = [item for item in _items]

        open = []

        if k != None:
            if int(k) > len(items):
                app.logger.debug("here")
                for item in items:
                    open.append(item['open_time'])
            else:
                stop = 0
                for item in items:
                    if stop < int(k):
                        open.append(item['open_time'])
                        stop += 1
                    else:
                        break
        else:
            for item in items:
                open.append(item['open_time'])

        return {'Open': open}


class OpenCSV(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument("top")
        args = parser.parse_args()
        k = args["top"]

        _items = db.myDB.find()
        items = [item for item in _items]

        open1 = []
        ret_string = "Open: \n"

        if k != None:
            if int(k) > len(items):
                for item in items:
                    open1.append(item['open_time'])
                    temp = item['open_time'].split(" ")
                    ret_string += (temp[0] + ',' + temp[1] + os.linesep)
            else:
                stop = 0
                for item in items:
                    if stop < int(k):
                        open1.append(item['open_time'])
                        temp = item['open_time'].split(" ")
                        ret_string += (temp[0] + ',' + temp[1] + os.linesep)
                        stop += 1
                    else:
                        break
        else:
            for item in items:
                open1.append(item['open_time'])
                temp = item['open_time'].split(" ")
                ret_string += (temp[0] + ',' + temp[1] + os.linesep)

        return Response(ret_string, mimetype="text/csv")


class OpenJSON(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument("top")
        args = parser.parse_args()
        k = args["top"]

        _items = db.myDB.find()
        items = [item for item in _items]

        open = []

        if k != None:
            if int(k) > len(items):
                app.logger.debug("here")
                for item in items:
                    open.append(item['open_time'])
            else:
                stop = 0
                for item in items:
                    if stop < int(k):
                        open.append(item['open_time'])
                        stop += 1
                    else:
                        break
        else:
            for item in items:
                open.append(item['open_time'])

        return {'Open': open}


class Close(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument("top")
        args = parser.parse_args()
        k = args["top"]

        _items = db.myDB.find()
        items = [item for item in _items]

        close = []

        if k != None:
            if int(k) > len(items):
                app.logger.debug("here")
                for item in items:
                    close.append(item['close_time'])
            else:
                stop = 0
                for item in items:
                    if stop < int(k):
                        close.append(item['close_time'])
                        stop += 1
                    else:
                        break
        else:
            for item in items:
                close.append(item['close_time'])

        return {'Close': close}


class CloseCSV(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument("top")
        args = parser.parse_args()
        k = args["top"]

        app.logger.debug("We got that k is: " + str(k))

        _items = db.myDB.find()
        items = [item for item in _items]

        ret_string = "Close: \n"

        if k != None:
            if int(k) > len(items):
                for item in items:
                    temp = item['close_time'].split(" ")
                    ret_string += (temp[0] + ',' + temp[1] + os.linesep)
            else:
                stop = 0
                for item in items:
                    if stop < int(k):
                        temp = item['close_time'].split(" ")
                        ret_string += (temp[0] + ',' + temp[1] + os.linesep)
                        stop += 1
                    else:
                        break;
        else:
            for item in items:
                temp = item['close_time'].split(" ")
                ret_string += (temp[0] + ',' + temp[1] + os.linesep)

        return Response(ret_string, mimetype="text/csv")


class CloseJSON(Resource):
    def get(self):
        # app.logger.debug("I got this number!: " + str(top))
        parser = reqparse.RequestParser()
        parser.add_argument("top")
        args = parser.parse_args()
        k = args["top"]

        _items = db.myDB.find()
        items = [item for item in _items]

        close = []

        if k != None:
            if int(k) > len(items):
                app.logger.debug("here")
                for item in items:
                    close.append(item['close_time'])
            else:
                stop = 0
                for item in items:
                    if stop < int(k):
                        close.append(item['close_time'])
                        stop += 1
                    else:
                        break
        else:
            for item in items:
                close.append(item['close_time'])

        return {'Close': close}


api.add_resource(OpenClose, '/listAll')
api.add_resource(OpenCloseCSV, '/listAll/csv')
api.add_resource(OpenCloseJSON, '/listAll/json')
api.add_resource(Open, '/listOpenOnly')
api.add_resource(OpenCSV, '/listOpenOnly/csv')
api.add_resource(OpenJSON, '/listOpenOnly/json')
api.add_resource(Close, '/listCloseOnly')
api.add_resource(CloseCSV, '/listCloseOnly/csv')
api.add_resource(CloseJSON, '/listCloseOnly/json')

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)



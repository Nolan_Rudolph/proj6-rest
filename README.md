Author: Nolan Rudolph  
Duck ID: 951553198  
E-mail: ngr@uoregon.edu  

Welcome to my newest software creation!  
Unfortunately, due to a lack of compatibility in the forwarding of projects, this must be a 200km race.  
You also must rewind time and start on January 1st, 2017. You must also start at midnight.  
Regardless...  
  
Usage! :  
1.) Simply input the times you wish into the time slot of index.html (You may neglect name and description)  
2.) There are now a variety of ways to access this data involving different inputs into the url.  
  
Here are a few things you can do:  
1.) Simply get the times:  
- http://<host:port>/listAll // This allows you to view both opening and closing times.  
- http://<host:port>/listOpenOnly // This allows you to view only opening times.  
- http://<host:port>/listCloseOnly // This allows you to view only closing times.  
2.) Obtain a CSV version of the times:  
- http://<host:port>/listAll/csv // This allows you to obtain the CSV of opening and closing times.  
- http://<host:port>/listOpenOnly/csv // This allows you to obtain the CSV of only opening times.  
- http://<host:port>/listCloseOnly/csv // This allows you to obtain the CSV of only closing times.  
3.) Obtain a JSON version of the times:  
- http://<host:port>/listAll/json // This allows you to obtain the JSON of opening and closing times.  
- http://<host:port>/listOpenOnly/json // This allows you to obtain the JSON of opening times.  
- http://<host:port>/listCloseOnly/json // This allows you to obtain the JSON of closing times.  
4.) Obtain a set amount of the times from any format:  
- http://<host:port>/listOpenOnly/csv?top=3 // Grab only the top 3 entries of the CSV for opening and closing times.  
- http://<host:port>/listCloseOnly/json?top=4 // Grab only the top 4 entries of the JSON for closing times.  
- http://<host:port>/listAll?top=10 // Grab only the top 10 entries of opening and closing times.  
  
Enjoy!  
